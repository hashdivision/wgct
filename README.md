Worldman Games Countdown Timer
=============================

**PyPI repository**: [wg-countdown-timer](https://pypi.org/project/wg-countdown-timer/)

This is the backend of countdown timer that is used at [Worldman Games](http://wmgames.ee) events.

Contributions (issues or merge requests) are welcome!
