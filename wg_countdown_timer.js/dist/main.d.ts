declare class Timer {
    private onTick;
    private _isStarted;
    readonly isStarted: boolean;
    private timeoutIndex;
    private startTime;
    private target;
    constructor(onTick: () => void);
    private tick;
    start(): void;
    stop(): void;
    restart(): void;
}
interface ITime {
    seconds: number;
    minutes: number;
    hours: number;
    tick(): void;
    update(seconds: number): void;
}
declare class Time implements ITime {
    protected _seconds: number;
    readonly seconds: number;
    protected _minutes: number;
    readonly minutes: number;
    protected _hours: number;
    readonly hours: number;
    constructor();
    tick(): void;
    update(seconds: number): void;
}
declare class StopwatchTime extends Time {
    constructor();
    tick(): void;
}
declare class CountdownTime extends Time {
    constructor();
    tick(): void;
}
interface CountdownTimerSettings {
    totalSecondsStartValue: number;
    secondsDecrements: Array<number>;
    totalRepeats: number;
    breaks: Array<number>;
    breakDuration: number;
    miniBreaks: Array<number>;
    miniBreakDuration: number;
}
declare class CountdownTimer {
    private onSecondPass;
    private onRepeatUpdate;
    private onRepeatRewind;
    private onTotalSecondsUpdate;
    private onBigBreak;
    private onMiniBreak;
    private onBreakEnd;
    private onPause;
    private onResume;
    private onWarmup;
    private onStart;
    private onStop;
    private onFinish;
    private onOneVersusOne;
    private timer;
    seconds: number;
    totalSeconds: number;
    totalSecondsStartValue: number;
    timePassed: StopwatchTime;
    secondsDecrements: number[];
    repeat: number;
    totalRepeats: number;
    breaks: number[];
    breakDuration: number;
    miniBreaks: number[];
    miniBreakDuration: number;
    readonly isBigBreak: boolean;
    readonly isMiniBreak: boolean;
    readonly isTotalSecondsDecrement: boolean;
    isRunning: boolean;
    isBreak: boolean;
    isPaused: boolean;
    untilBreak: CountdownTime;
    constructor(onSecondPass: (seconds: number, timePassed: StopwatchTime, untilBreak: CountdownTime) => void, onRepeatUpdate: (repeat: number) => void, onRepeatRewind: (repeat: number) => void, onTotalSecondsUpdate: (totalSeconds: number) => void, onBigBreak: () => void, onMiniBreak: () => void, onBreakEnd: () => void, onPause: () => void, onResume: () => void, onWarmup: () => void, onStart: () => void, onStop: () => void, onFinish: () => void, onOneVersusOne: () => void);
    private setUp;
    private calculateUntilBreak;
    private loop;
    start(settings: CountdownTimerSettings): void;
    pause(): void;
    resume(): void;
    stop(): void;
    rewindRepeat(): void;
    rewind(): void;
    oneVersusOne(): void;
}
export { Timer, ITime, Time, StopwatchTime, CountdownTime, CountdownTimer, CountdownTimerSettings };
