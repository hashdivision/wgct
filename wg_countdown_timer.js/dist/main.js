"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Timer = (function () {
    function Timer(onTick) {
        this.onTick = onTick;
        this._isStarted = false;
        this.timeoutIndex = -1;
    }
    Object.defineProperty(Timer.prototype, "isStarted", {
        get: function () { return this._isStarted; },
        enumerable: true,
        configurable: true
    });
    Timer.prototype.tick = function () {
        if (!this.isStarted) {
            this.startTime = new Date().valueOf();
            this.target = 1000;
            this._isStarted = true;
            this.timeoutIndex = window.setTimeout(this.tick.bind(this), 1000);
        }
        else {
            var elapsed = new Date().valueOf() - this.startTime;
            var adjust = this.target - elapsed;
            this.target += 1000;
            this.timeoutIndex = window.setTimeout(this.tick.bind(this), 1000 + adjust);
            this.onTick();
        }
    };
    Timer.prototype.start = function () {
        if (this.isStarted) {
            return;
        }
        this.tick();
    };
    Timer.prototype.stop = function () {
        if (this.isStarted) {
            window.clearTimeout(this.timeoutIndex);
            this._isStarted = false;
        }
    };
    Timer.prototype.restart = function () {
        this.stop();
        this.start();
    };
    return Timer;
}());
exports.Timer = Timer;
var Time = (function () {
    function Time() {
        this._seconds = 0;
        this._minutes = 0;
        this._hours = 0;
    }
    Object.defineProperty(Time.prototype, "seconds", {
        get: function () { return this._seconds; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Time.prototype, "minutes", {
        get: function () { return this._minutes; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Time.prototype, "hours", {
        get: function () { return this._hours; },
        enumerable: true,
        configurable: true
    });
    Time.prototype.tick = function () { };
    Time.prototype.update = function (seconds) {
        this._seconds = seconds % 60;
        this._minutes = Math.floor(seconds / 60) % 60;
        this._hours = Math.floor(seconds / 60 / 60);
    };
    return Time;
}());
exports.Time = Time;
var StopwatchTime = (function (_super) {
    __extends(StopwatchTime, _super);
    function StopwatchTime() {
        return _super.call(this) || this;
    }
    StopwatchTime.prototype.tick = function () {
        this._seconds++;
        if (this.seconds === 60) {
            this._seconds %= 60;
            this._minutes++;
            if (this.minutes === 60) {
                this._minutes %= 60;
                this._hours++;
            }
        }
    };
    return StopwatchTime;
}(Time));
exports.StopwatchTime = StopwatchTime;
var CountdownTime = (function (_super) {
    __extends(CountdownTime, _super);
    function CountdownTime() {
        return _super.call(this) || this;
    }
    CountdownTime.prototype.tick = function () {
        this._seconds--;
        if (this.seconds >= 0)
            return;
        if (this.minutes > 0) {
            this._seconds = 59;
            this._minutes--;
        }
        else if (this.hours > 0) {
            this._seconds = 59;
            this._minutes = 59;
            this._hours--;
        }
        else {
            this._seconds = 0;
        }
    };
    return CountdownTime;
}(Time));
exports.CountdownTime = CountdownTime;
var DefaultSettings = (function () {
    function DefaultSettings() {
        this.totalSecondsStartValue = 0;
        this.secondsDecrements = null;
        this.totalRepeats = 0;
        this.breaks = null;
        this.breakDuration = 0;
        this.miniBreaks = null;
        this.miniBreakDuration = 0;
    }
    return DefaultSettings;
}());
var CountdownTimer = (function () {
    function CountdownTimer(onSecondPass, onRepeatUpdate, onRepeatRewind, onTotalSecondsUpdate, onBigBreak, onMiniBreak, onBreakEnd, onPause, onResume, onWarmup, onStart, onStop, onFinish, onOneVersusOne) {
        this.onSecondPass = onSecondPass;
        this.onRepeatUpdate = onRepeatUpdate;
        this.onRepeatRewind = onRepeatRewind;
        this.onTotalSecondsUpdate = onTotalSecondsUpdate;
        this.onBigBreak = onBigBreak;
        this.onMiniBreak = onMiniBreak;
        this.onBreakEnd = onBreakEnd;
        this.onPause = onPause;
        this.onResume = onResume;
        this.onWarmup = onWarmup;
        this.onStart = onStart;
        this.onStop = onStop;
        this.onFinish = onFinish;
        this.onOneVersusOne = onOneVersusOne;
        this.timer = new Timer(this.loop.bind(this));
        this.seconds = 0;
        this.totalSeconds = 0;
        this.totalSecondsStartValue = 0;
        this.timePassed = new StopwatchTime();
        this.secondsDecrements = null;
        this.repeat = 0;
        this.totalRepeats = 0;
        this.breaks = null;
        this.breakDuration = 0;
        this.miniBreaks = null;
        this.miniBreakDuration = 0;
        this.isRunning = false;
        this.isBreak = false;
        this.isPaused = false;
        this.untilBreak = new CountdownTime();
    }
    Object.defineProperty(CountdownTimer.prototype, "isBigBreak", {
        get: function () { return this.breaks.indexOf(this.repeat) != -1; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CountdownTimer.prototype, "isMiniBreak", {
        get: function () { return this.miniBreaks.indexOf(this.repeat) != -1; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CountdownTimer.prototype, "isTotalSecondsDecrement", {
        get: function () { return this.secondsDecrements.indexOf(this.repeat) != -1; },
        enumerable: true,
        configurable: true
    });
    CountdownTimer.prototype.setUp = function (settings) {
        this.isRunning = false;
        this.isBreak = false;
        this.isPaused = false;
        this.seconds = 15;
        this.totalSeconds = settings.totalSecondsStartValue;
        this.totalSecondsStartValue = settings.totalSecondsStartValue;
        this.timePassed.update(0);
        this.secondsDecrements = settings.secondsDecrements;
        this.repeat = -1;
        this.totalRepeats = settings.totalRepeats;
        this.breaks = settings.breaks;
        this.breakDuration = settings.breakDuration;
        this.miniBreaks = settings.miniBreaks;
        this.miniBreakDuration = settings.miniBreakDuration;
        this.untilBreak.update(0);
    };
    CountdownTimer.prototype.calculateUntilBreak = function () {
        if (this.isBreak && this.isBigBreak) {
            this.untilBreak.update(0);
            return;
        }
        var seconds = this.seconds;
        var i = 0;
        for (i = 0; i < this.breaks.length; i++) {
            if (this.repeat < this.breaks[i]) {
                break;
            }
            if (i === this.breaks.length - 1) {
                this.untilBreak.update(0);
                return;
            }
        }
        var repeatsUntilBreak = this.breaks[i] - this.repeat - 1;
        if (i < this.miniBreaks.length && this.repeat < this.miniBreaks[i]) {
            seconds += this.miniBreakDuration;
            if (this.secondsDecrements[i] === this.miniBreaks[i]) {
                var repeatsUntilMiniBreak = this.miniBreaks[i] - this.repeat - 1;
                seconds += (this.totalSeconds - 1) * (repeatsUntilBreak - repeatsUntilMiniBreak);
                repeatsUntilBreak = repeatsUntilMiniBreak;
            }
        }
        this.untilBreak.update(seconds + repeatsUntilBreak * this.totalSeconds);
    };
    CountdownTimer.prototype.loop = function () {
        this.seconds--;
        this.timePassed.tick();
        if (!this.isBreak || this.isMiniBreak) {
            this.untilBreak.tick();
        }
        if (this.seconds > 0) {
            this.onSecondPass(this.seconds, this.timePassed, this.untilBreak);
            return;
        }
        if (this.isBreak) {
            this.seconds = this.totalSeconds;
            this.isBreak = false;
            this.calculateUntilBreak();
            this.onSecondPass(this.seconds, this.timePassed, this.untilBreak);
            this.onBreakEnd();
            return;
        }
        this.repeat++;
        if (this.repeat === this.totalRepeats) {
            this.timer.stop();
            this.isRunning = false;
            this.onSecondPass(this.seconds, this.timePassed, this.untilBreak);
            this.onRepeatUpdate(this.repeat);
            this.onFinish();
            return;
        }
        if (this.isTotalSecondsDecrement) {
            this.totalSeconds--;
            this.onTotalSecondsUpdate(this.totalSeconds);
        }
        this.seconds = this.totalSeconds;
        if (this.isBigBreak) {
            this.seconds = this.breakDuration;
            this.isBreak = true;
            this.onBigBreak();
        }
        else if (this.isMiniBreak) {
            this.seconds = this.miniBreakDuration;
            this.isBreak = true;
            this.onMiniBreak();
        }
        this.onSecondPass(this.seconds, this.timePassed, this.untilBreak);
        this.onRepeatUpdate(this.repeat);
        if (this.repeat === 0) {
            this.onStart();
        }
    };
    CountdownTimer.prototype.start = function (settings) {
        if (this.isRunning) {
            return;
        }
        this.setUp(settings);
        this.isRunning = true;
        this.breaks.sort(function (a, b) { return a - b; });
        this.miniBreaks.sort(function (a, b) { return a - b; });
        this.calculateUntilBreak();
        this.onSecondPass(this.seconds, this.timePassed, this.untilBreak);
        this.onTotalSecondsUpdate(this.totalSeconds);
        this.onRepeatUpdate(this.repeat);
        this.timer.start();
        this.onWarmup();
    };
    CountdownTimer.prototype.pause = function () {
        if (!this.isRunning || this.isPaused) {
            return;
        }
        this.timer.stop();
        this.isPaused = true;
        this.onPause();
    };
    CountdownTimer.prototype.resume = function () {
        if (!this.isRunning || !this.isPaused) {
            return;
        }
        this.timer.start();
        this.isPaused = false;
        this.onResume();
    };
    CountdownTimer.prototype.stop = function () {
        if (!this.isRunning) {
            return;
        }
        this.timer.stop();
        this.setUp(new DefaultSettings());
        this.onStop();
    };
    CountdownTimer.prototype.rewindRepeat = function () {
        if (this.isBigBreak || this.isMiniBreak || this.isTotalSecondsDecrement) {
            return;
        }
        this.repeat--;
        this.rewind();
        this.onRepeatRewind(this.repeat);
    };
    CountdownTimer.prototype.rewind = function () {
        if (!this.isRunning) {
            return;
        }
        this.pause();
        this.seconds = this.totalSeconds;
        this.calculateUntilBreak();
        this.onSecondPass(this.seconds, this.timePassed, this.untilBreak);
    };
    CountdownTimer.prototype.oneVersusOne = function () {
        if (!this.isRunning) {
            return;
        }
        this.rewindRepeat();
        this.seconds = this.breakDuration;
        this.isBreak = true;
        this.resume();
        this.onOneVersusOne();
    };
    return CountdownTimer;
}());
exports.CountdownTimer = CountdownTimer;
//# sourceMappingURL=main.js.map