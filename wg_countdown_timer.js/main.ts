class Timer {
    private _isStarted: boolean = false
    public get isStarted(): boolean { return this._isStarted }

    private timeoutIndex: number = -1
    private startTime: number
    private target: number

    constructor(private onTick: () => void) { }
    
    private tick() {
        if (!this.isStarted) {
            this.startTime = new Date().valueOf()
            this.target = 1000
            this._isStarted = true
            this.timeoutIndex = window.setTimeout(this.tick.bind(this), 1000)
        } else {
            const elapsed = new Date().valueOf() - this.startTime
            const adjust = this.target - elapsed
            this.target += 1000
            this.timeoutIndex = window.setTimeout(this.tick.bind(this), 1000 + adjust)
            this.onTick()
        }
    }

    public start() {
        if (this.isStarted) { return }
        this.tick()
    }

    public stop() {
        if (this.isStarted) {
            window.clearTimeout(this.timeoutIndex)
            this._isStarted = false
        }
    }

    public restart() {
        this.stop()
        this.start()
    }
}

interface ITime {
    seconds: number
    minutes: number
    hours: number

    tick(): void
    update(seconds: number): void
}

class Time implements ITime {
    protected _seconds: number = 0
    public get seconds(): number { return this._seconds }

    protected _minutes: number = 0
    public get minutes(): number { return this._minutes }

    protected _hours: number = 0
    public get hours(): number { return this._hours }

    constructor() { }

    public tick() { }

    public update(seconds: number) {
        this._seconds = seconds % 60
        this._minutes = Math.floor(seconds/60) % 60
        this._hours = Math.floor(seconds/60/60)
    }
}

class StopwatchTime extends Time {
    constructor() { super() }

    public tick() {
        this._seconds++
        if (this.seconds === 60) {
            // Update minutes and mod seconds
            this._seconds %= 60
            this._minutes++
            if (this.minutes === 60) {
                // Update hours and mod minutes
                this._minutes %= 60
                this._hours++
            }
        }
    }
}

class CountdownTime extends Time {
    constructor() { super() }

    public tick() {
        this._seconds--
        if (this.seconds >= 0) return

        // No seconds left to count down

        if (this.minutes > 0) {
            // Restore seconds from minutes
            this._seconds = 59
            this._minutes--
        } else if (this.hours > 0) {
            // Restore minutes and seconds from hours
            this._seconds = 59
            this._minutes = 59
            this._hours--
        } else {
            // No time left to count down
            this._seconds = 0
        }
    }
}

interface CountdownTimerSettings {
    totalSecondsStartValue: number
    secondsDecrements: Array<number>
    totalRepeats: number
    breaks: Array<number>
    breakDuration: number
    miniBreaks: Array<number>
    miniBreakDuration: number
}

class DefaultSettings implements CountdownTimerSettings {
    public totalSecondsStartValue = 0
    public secondsDecrements = <number[]>null
    public totalRepeats = 0
    public breaks = <number[]>null
    public breakDuration = 0
    public miniBreaks = <number[]>null
    public miniBreakDuration = 0
}

class CountdownTimer {
    private timer: Timer = new Timer(this.loop.bind(this));

    public seconds: number = 0 // How many seconds left to make a repeat
    public totalSeconds: number = 0 // Total seconds for one repeat
    public totalSecondsStartValue: number = 0 // Start value of totalSeconds for one report (totalSeconds will change on secondsDecrements repeats)
    public timePassed: StopwatchTime = new StopwatchTime() // Time passed from the start
    public secondsDecrements: number[] = null // Repeats after which totalSeconds will be decreased by 1

    public repeat: number = 0 // Current repeat
    public totalRepeats: number = 0 // Total number of repeats

    public breaks: number[] = null // Repeats after which there will be a break
    public breakDuration: number = 0 // How many seconds a break will last
    public miniBreaks: number[] = null // Repeats after which there will be a mini-break to prepare for total_seconds change
    public miniBreakDuration: number = 0 // How many seconds a mini-break will last

    public get isBigBreak() : boolean { return this.breaks.indexOf(this.repeat) != -1 }
    public get isMiniBreak() : boolean { return this.miniBreaks.indexOf(this.repeat) != -1 }
    public get isTotalSecondsDecrement() : boolean { return this.secondsDecrements.indexOf(this.repeat) != -1 }

    public isRunning: boolean = false // Is CountdownTimer running
    public isBreak: boolean = false // Is it big break or mini-break right now
    public isPaused: boolean = false // Is CountdownTimer paused

    public untilBreak: CountdownTime = new CountdownTime() // How much time still left until the big break

    constructor(private onSecondPass: (seconds: number, timePassed: StopwatchTime, untilBreak: CountdownTime) => void, // Called every second
                private onRepeatUpdate: (repeat: number) => void, // Called when repeat value is changed
                private onRepeatRewind: (repeat: number) => void, // Called when repeat is rewinded to previous
                private onTotalSecondsUpdate: (totalSeconds: number) => void, // Called after total_seconds value is changed
                private onBigBreak: () => void, // Called when big break starts
                private onMiniBreak: () => void, // Called when mini-break starts
                private onBreakEnd: () => void, // Called when any break ends
                private onPause: () => void, // Called when CountdownTimer is paused
                private onResume: () => void, // Called when CountdownTimer is resumed
                private onWarmup: () => void, // Called when CountdownTimer is started
                private onStart: () => void, // Called when warm-up is finished
                private onStop: () => void, // Called when CountdownTimer is stopped
                private onFinish: () => void, // Called when CountdownTimer is finished
                private onOneVersusOne: () => void // Called when 1 VS 1 mode is enabled
        ) { }
    
    private setUp(settings: CountdownTimerSettings) {
        this.isRunning = false
        this.isBreak = false
        this.isPaused = false
        this.seconds = 15 // Warm-up is 15 seconds
        this.totalSeconds = settings.totalSecondsStartValue
        this.totalSecondsStartValue = settings.totalSecondsStartValue
        this.timePassed.update(0)
        this.secondsDecrements = settings.secondsDecrements
        this.repeat = -1 // Warm-up repeat
        this.totalRepeats = settings.totalRepeats
        this.breaks = settings.breaks
        this.breakDuration = settings.breakDuration
        this.miniBreaks = settings.miniBreaks
        this.miniBreakDuration = settings.miniBreakDuration
        this.untilBreak.update(0)
    }
    
    private calculateUntilBreak() {
        // When we have big break - we do not calculate until_break 
        if (this.isBreak && this.isBigBreak) {
            this.untilBreak.update(0)
            return
        }

        let seconds = this.seconds

        // Find out what is the closest break point
        let i = 0;
        for (i = 0; i < this.breaks.length; i++) {
            if (this.repeat < this.breaks[i]) {
                break
            }

            // Finish line - there is no breaks left
            if (i === this.breaks.length-1) {
                this.untilBreak.update(0)
                return
            }
        }

        let repeatsUntilBreak = this.breaks[i] - this.repeat - 1

        // Account for mini-break
        if (i < this.miniBreaks.length && this.repeat < this.miniBreaks[i]) {
            seconds += this.miniBreakDuration
            if (this.secondsDecrements[i] === this.miniBreaks[i]) {
                const repeatsUntilMiniBreak = this.miniBreaks[i] - this.repeat - 1
                seconds += (this.totalSeconds-1) * (repeatsUntilBreak - repeatsUntilMiniBreak)
                repeatsUntilBreak = repeatsUntilMiniBreak
            }
        }

        this.untilBreak.update(seconds + repeatsUntilBreak * this.totalSeconds)
    }

    private loop() {
        this.seconds--
        this.timePassed.tick()
        if (!this.isBreak || this.isMiniBreak) {
            // We tick until_break when it is mini-break or no break at all
            this.untilBreak.tick()
        }

        // Exit immediately if there are still seconds for this repeat
        if (this.seconds > 0) {
            this.onSecondPass(this.seconds, this.timePassed, this.untilBreak)
            return
        }

        /**
         * There are no seconds left for this repeat:
         */

        // If we were having a break - stop it and proceed without incrementing repeat counter
        if (this.isBreak) {
            this.seconds = this.totalSeconds
            this.isBreak = false
            this.calculateUntilBreak()
            this.onSecondPass(this.seconds, this.timePassed, this.untilBreak)
            this.onBreakEnd()
            return
        }

        this.repeat++
        // If this repeat is the last one - stop everything and exit
        if (this.repeat === this.totalRepeats) {
            this.timer.stop()
            this.isRunning = false
            this.onSecondPass(this.seconds, this.timePassed, this.untilBreak)
            this.onRepeatUpdate(this.repeat)
            this.onFinish()
            return
        }

        // Check if total_seconds should be decremented after this repeat
        if (this.isTotalSecondsDecrement) {
            this.totalSeconds--
            this.onTotalSecondsUpdate(this.totalSeconds)
        }

        this.seconds = this.totalSeconds
        // Check if any of the breaks should be after this repeat
        if (this.isBigBreak) {
            this.seconds = this.breakDuration
            this.isBreak = true
            this.onBigBreak()
        } else if (this.isMiniBreak) {
            this.seconds = this.miniBreakDuration
            this.isBreak = true
            this.onMiniBreak()
        }

        this.onSecondPass(this.seconds, this.timePassed, this.untilBreak)
        this.onRepeatUpdate(this.repeat)

        if (this.repeat === 0) { this.onStart() }
    }

    public start(settings: CountdownTimerSettings) {
        // Start only works if CountdownTimer is not running
        if (this.isRunning) { return }

        // Reset everything to start values
        this.setUp(settings)
        this.isRunning = true
        this.breaks.sort(function(a, b) { return a-b })
        this.miniBreaks.sort(function(a, b) { return a-b })
        this.calculateUntilBreak()

        this.onSecondPass(this.seconds, this.timePassed, this.untilBreak)
        this.onTotalSecondsUpdate(this.totalSeconds)
        this.onRepeatUpdate(this.repeat)

        this.timer.start()
        this.onWarmup()
    }

    public pause() {
        // Pause only works if CountdownTimer is running and not paused
        if (!this.isRunning || this.isPaused) { return }

        this.timer.stop()
        this.isPaused = true
        this.onPause()
    }

    public resume() {
        // Resume only works if CountdownTimer is running and paused
        if (!this.isRunning || !this.isPaused) { return }

        this.timer.start()
        this.isPaused = false
        this.onResume()
    }

    public stop() {
        // Stop only works if CountdownTimer is running
        if (!this.isRunning) { return }

        this.timer.stop()
        this.setUp(new DefaultSettings())
        this.onStop()
    }

    public rewindRepeat() {
        // Rewind to previous repeat just before break or totalSeconds decrement is restricted
        if (this.isBigBreak || this.isMiniBreak || this.isTotalSecondsDecrement) { return }

        // Rewind to previous repeat
        this.repeat--
        this.rewind()
        this.onRepeatRewind(this.repeat)
    }

    public rewind() {
        // Rewind current repeat only works if CountdownTimer is running
        if (!this.isRunning) { return }

        this.pause()

        // Rewind to start of current repeat
        this.seconds = this.totalSeconds
        this.calculateUntilBreak()
        this.onSecondPass(this.seconds, this.timePassed, this.untilBreak)
    }

    public oneVersusOne() {
        // 1 VS 1 mode only works if CountdownTimer is running
        if (!this.isRunning) { return }

        this.rewindRepeat()
        this.seconds = this.breakDuration
        this.isBreak = true
        this.resume()
        this.onOneVersusOne()
    }
}

export { Timer, ITime, Time, StopwatchTime, CountdownTime, CountdownTimer, CountdownTimerSettings }
